#! python3

# Tools for translating Triva Lapur

import os, sys, csv, random, pygame, re, datetime, shelve
from PIL import Image as PILImage
from tkinter import *
import tkinter.messagebox

def translit():
    global select
    global transclst

    ip = gloss.get()
    
    wordlist = ip.replace('-', ' -').split()

    for i in choice:
        if i[0] not in wordlist or i[1] == None:
            choice.remove(i)

    shelfFile = shelve.open('mydata')
    shelfFile['choice'] = choice
    shelfFile.close()

    glossstr = ''
    transclst = []

    for i in wordlist:
        if not i.isupper() and not i.startswith('-'):
            i = i.replace('.', ' ')
            found = []
            for j in dictlist:
                deflist = j[3].split(', ')
                for m in range(len(deflist)):
                    if '(' in deflist[m]:
                        deflist += deflist[m].split(' (')
                        deflist += deflist[m].split(') ')
                        deflist[m] = deflist[m].replace('(', '')
                        deflist[m] = deflist[m].replace(')', '')
                if i in deflist:
                    found.append(j)
            if len(found) == 0:
                glossstr += ' [' + i + ']'
                transclst.append('kuum.png')
            elif len(found) == 1:
                glossstr += ' ' + found[0][0]
                transclst.append(found[0][1])
            elif len(found) > 1:
                choicefound = False
                for k in choice:
                    if i == k[0]:
                        glossstr += ' ' + found[k[1]][0]
                        transclst.append(found[k[1]][1])
                        choicefound = True
                if not choicefound:
                    foundtext = i + '\n'
                    for l in range(len(found)):
                        foundtext += str(l) + ': ' + found[l][0] + ' ' + found[l][2] + '\n'
                    popup = Toplevel(root)
                    foundLabel = Label(popup, text = foundtext)
                    foundLabel.pack()
                    select = Entry(popup)
                    select.pack()
                    selectButton = Button(popup, text = 'select', command = choose)
                    selectButton.pack()
                    choice.append([i, None])
                    glossstr += ' [' + i + ']'
                    transclst.append('kuum.png')
        else:
            for j in gram:
                if i == j[2]:
                    lst = j[0].split()
                    for m in lst:
                        transclst.append(m + '.png')
                    if i.startswith('-'):
                        if glossstr[-1].endswith(vowels):
                            if j[3].startswith(vowels):
                                glossstr = glossstr[0:-1]
                            glossstr += j[3]
                        else:
                            glossstr += j[1]
                    else:
                        if j[1] == '':
                            glossstr += ''
                        else:
                            glossstr += ' ' + j[1]

    glossstr = lengthen(glossstr.strip())

    init = glossstr[0]

    glossstr = glossstr[1:]

    glossstr = init.upper() + glossstr

    tl.set(glossstr + '.')

    shelfFile = shelve.open('mydata')
    shelfFile['gloss'] = ip
    shelfFile['sentence'] = sentence.get()
    shelfFile.close()

def choose():
    choice[-1][1] = int(select.get())
    shelfFile = shelve.open('mydata')
    shelfFile['choice'] = choice
    shelfFile.close()
    translit()

def lengthen(x):
    for i in length.keys():
        if i in x:
            subst = re.compile(i)
            x = subst.sub(length[i], x)
    return(x)

def entry(x):
    for i in dictlist:
        if x == i[0]:
            return(i)

def generate():
    rule = []

    if random.random() > 0.5:
        rule.append(c)
        rule.append(r)
    else:
        rule.append(k)

    rule.append(v)
    rule.append(k)

    if random.random() > 0.5:
        rule.append(c)
        rule.append(r)
    else:
        rule.append(k)

    rule.append(v)
    rule.append(k)

    output = ''
    
    for i in rule:
        output += i[random.randint(0, len(i) - 1)]

    rand.set(output)

def expdict(dictlist):
    dictlist = sorted(dictlist, key=lambda x: x[0].strip('-').lower())

    lexicon = open('lexicon.csv', 'w')
    writer = csv.writer(lexicon)
    for i in dictlist:
        writer.writerow(i)
    lexicon.close()

    umlex = open('../neocities/umlexicon.html')
    lexlist = umlex.readlines()
    umlex.close()
    umlex = open('../neocities/umlexicon.html', 'w')
    umlex.write('')
    umlex.close()
    umlex = open('../neocities/umlexicon.html', 'a')
    for i in range(77):
        umlex.write(lexlist[i])

    for i in dictlist:
        entry = lengthen(i[0])

        if i[2] == 'v.':
            entry += '-'

        umlex.write('<tr>\n<th>' + entry + '</th>\n<td>')

        umlex.write('<img src="redscript/' + i[1] + '"></td>\n<td>' + i[2] +
                    '</td>\n<td>')

        deflist = i[3].split(', ')

        for j in range(len(deflist)):
            if j == len(deflist) - 1:
                umlex.write(deflist[j] + '</td>\n</tr>\n\n')
            else:
                umlex.write(deflist[j] + ',<br>')

    umlex.write('</table>\n\n<p><br><br><br><br></p>\n\n</div>\n\n</body>\n</html>')

    umlex.close()

def transc():
    output = PILImage.new('RGBA', (15 * len(transclst), 12))
    width = 0
    
    for i in transclst:
        image = PILImage.open('./script/' + i)
        output.paste(image, (width, 0))
        iwidth, iheight = image.size
        width += iwidth

    cropoutput = output.crop((3, 0, width, 12))
    cropoutput.save('temp.png', dpi = (72, 72))

    transcript = PhotoImage(file = 'temp.png')
    
    transcrDisplay.configure(image = transcript)

    transcrDisplay.image = transcript

def genchar(name):
    pygame.init()

    win = pygame.display.set_mode((12*24, 12*24))

    pygame.display.set_caption('Triva')

    matrix = []
    hover_x = 0
    hover_y = 0

    for y in range(12):
        line = []
        for x in range(12):
            line.append(0)
        matrix.append(list(line))

    fps = pygame.time.Clock()

    while True:
        fps.tick(30)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.display.quit()
                output = PILImage.new('RGBA', (15, 12))
                redoutput = PILImage.new('RGBA', (15, 12))
                for x in range(12):
                    for y in range(12):
                        if matrix[x][y] == 1:
                            output.putpixel((x + 3, y), (0, 0, 0, 255))
                            redoutput.putpixel((x + 3, y), (214, 47, 69, 255))
                output.save('./script/' + name)
                resized = redoutput.resize((30, 24), resample=0)
                resized.save('../neocities/redscript/' + name)
                return()
            if event.type == pygame.MOUSEMOTION:
                hover_x, hover_y = pygame.mouse.get_pos()
            if event.type == pygame.MOUSEBUTTONDOWN:
                mx, my = pygame.mouse.get_pos()
                if matrix[mx // 24][my // 24] == 0:
                    matrix[mx // 24][my // 24] = 1
                else:
                    matrix[mx // 24][my // 24] = 0

        pygame.draw.rect(win, (255, 255, 255), (0, 0, 12 * 24, 12 * 24))
        pygame.draw.rect(win, (0,0,0), ((hover_x//24)*24, (hover_y//24)*24,  24, 24), 2)

        for x in range(12):
            for y in range(12):
                if matrix[x][y] == 1:
                    pygame.draw.rect(win, (0,0,0), (x*24, y*24, 24, 24))

        pygame.display.update()

def blog():
    global imagename
    global blogTitle
    
    popup = Toplevel(root)
    
    imageLabel = Label(popup, text = 'image name:')
    imageLabel.grid(row = 0, column = 0, sticky = E, padx = 10)

    imagename = Entry(popup, width = 30)
    imagename.grid(row = 0, column = 1)

    titleLabel = Label(popup, text = 'title:')
    titleLabel.grid(row = 1, column = 0, sticky = E, padx = 10)

    blogTitle = Entry(popup, width = 30)
    blogTitle.grid(row = 1, column = 1)

    blogButton = Button(popup, text = 'blog', command = blogexport)
    blogButton.grid(row = 1, column = 2, padx = 5)

    exportButton = Button(popup, text = 'export', command = exportexport)
    exportButton.grid(row = 1, column = 3, padx = 5)

def exportexport():
    image = imagename.get()

    imagefile = PILImage.open('temp.png')

    imagefile.save('./export/' + image + '.png')

    title = blogTitle.get()

    export = open('./export/' + title + '.txt', 'w')

    export.write(tl.get() + '\n' + sentence.get())

    export.close()

def blogexport():
    image = imagename.get()

    imagefile = PILImage.open('temp.png')

    width, height = imagefile.size
    
    for x in range(width):
        for y in range(height):
            if imagefile.getpixel((x, y)) == (0, 0, 0, 255):
                imagefile.putpixel((x, y), (0, 128, 0, 255))

    resized = imagefile.resize((width * 4, height * 4), resample=0)
    
    resized.save('../neocities/' + image + '.png')
    
    blog = open('../neocities/blog.html')
    bloglist = blog.readlines()
    blog.close()
    blog = open('../neocities/blog.html', 'w')
    blog.write('')
    blog.close()
    blog = open('../neocities/blog.html', 'a')
    for i in range(63):
        blog.write(bloglist[i])

    title = blogTitle.get()

    dt = datetime.datetime.now()

    date = dt.strftime('%d.%m.%Y')

    blog.write('<h2><a name="' + title +'">' + date +': ' + title +
               '</a></h2>\n')
    blog.write('<img src="' + image + '.png">\n')

    blog.write('<p><b>' + tl.get() + '</b>\n')

    blog.write('<br>' + sentence.get() + '</p>\n\n')

    for i in range(63, len(bloglist)):
        blog.write(bloglist[i])
    blog.close()

    rss = open('../neocities/rss.xml')
    rsslist = rss.readlines()
    rss.close()
    rss = open('../neocities/rss.xml', 'w')
    rss.write('')
    rss.close()
    rss = open('../neocities/rss.xml', 'a')
    for i in range(len(rsslist) - 2):
        rss.write(rsslist[i])

    rss.write('<item>\n')
    rss.write('<title>' + title + '</title>\n')
    rss.write('<link>https://cenysor.neocities.org/blog.html#' + title +
              '</link>\n')
    rss.write('<guid>https://cenysor.neocities.org/blog.html#' + title +
              '</guid>\n')

    pubdate = dt.strftime('%a, %d %b %Y %H:%M:%S')
    
    rss.write('<pubDate>' + pubdate + ' GMT</pubDate>\n')
    rss.write('<description>Translation</description>\n</item>\n</channel>\n</rss>')
    rss.close()

def add():
    x = dictentry.get()
    g = grammar.get()
    c = char.get() + '.png'
    d = definition.get()

    entryfound = False
    
    for i in dictlist:
        if x == i[0]:
            if g == '' or g == i[2]:
                i[3] += (', ' + d)
                entryfound = True
                break
            elif c == '.png' or c == i[1]:
                tkinter.messagebox.showinfo('Please enter a different character name!')
                return()
        
    if entryfound == False:
        if c == '.png':
            c = x + '.png'
            genchar(c)
        else:
            charfound = False
            for i in dictlist:
                if c == i[1]:
                    charfound = True
                    break
            if charfound == False:
                genchar(c)
        dictlist.append([x, c, g, d, ''])

        image = PILImage.open('./script/' + c)

        blank = True

        try:
            for i in range(12):
                if not image.getpixel((14, i)) == (0, 0, 0, 0):
                    blank = False
                    break

            if blank:
                image = image.crop((0, 0, 14, 12))
                image.save('./script/' + c)
        except IndexError:
            pass

        binary = ''

        for ix in range(15):
            for iy in range(12):
                try:
                    pixel = image.getpixel((ix, iy))
                except IndexError:
                    break
                if pixel == (0, 0, 0, 255):
                    binary += '1'
                else:
                    binary += '0'

        dictlist[-1][4] = binary

        for i in dictlist:
            try:
                if binary == i[4] and c != i[1]:
                    tkinter.messagebox.showinfo('Info', x + ' uses the same character as ' + i[0])
            except IndexError:
                continue

    expdict(dictlist)
    translit()

def delete():
    dictlist.remove(entry(dictentry.get()))
    expdict(dictlist)

root = Tk()

root.title('Triva')

w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry('%dx%d+0+0' % (w, h))

gram = [['3 1 PL GEN', 'rama', '1PL.POSS'],
        ['3 2 PL GEN', 'enma', '2PL.POSS'],
        ['3 PL GEN', 'ama', '3PL.POSS'],
        ['3 1 GEN', 'arim', '1SG.POSS'],
        ['3 2 GEN', 'anam', '2SG.POSS'],
        ['3 GEN', 'ayam', '3SG.POSS'],
        ['3 1 PL ACC', 'lara', '1PL.ACC'],
        ['3 2 PL ACC', 'enra', '2PL.ACC'],
        ['3 PL ACC', 'yara', '3PL.ACC'],
        ['3 1 ACC', 'arir', '1SG.ACC'],
        ['3 2 ACC', 'anar', '2SG.ACC'],
        ['3 ACC', 'ayar', '3SG.ACC'],
        ['3 1 PL DAT', 'rana', '1PL.DAT'],
        ['3 2 PL DAT', 'nana', '2PL.DAT'],
        ['3 PL DAT', 'yana', '3PL.DAT'],
        ['3 1 DAT', 'arin', '1SG.DAT'],
        ['3 2 DAT', 'anan', '2SG.DAT'],
        ['3 DAT', 'ayan', '3SG.DAT'],
        ['3 1 PL LOC', 'rima', '1PL.LOC'],
        ['3 2 PL LOC', 'inma', '2PL.LOC'],
        ['3 PL LOC', 'ima', '3PL.LOC'],
        ['3 1 LOC', 'irim', '1SG.LOC'],
        ['3 2 LOC', 'anim', '2SG.LOC'],
        ['3 LOC', 'ayim', '3SG.LOC'],
        ['3 1 PL ABL', 'lura', '1PL.ABL'],
        ['3 2 PL ABL', 'unra', '2PL.ABL'],
        ['3 PL ABL', 'ura', '3PL.ABL'],
        ['3 1 ABL', 'alur', '1SG.ABL'],
        ['3 2 ABL', 'anur', '2SG.ABL'],
        ['3 ABL', 'ayur', '3SG.ABL'],
        ['3 1 PL ISTR', 'lira', '1PL.COM'],
        ['3 2 PL ISTR', 'inra', '2PL.COM'],
        ['3 PL ISTR', 'ira', '3PL.COM'],
        ['3 1 ISTR', 'alir', '1SG.COM'],
        ['3 2 ISTR', 'anir', '2SG.COM'],
        ['3 ISTR', 'ayir', '3SG.COM'],
        ['DEM PL GEN', 'vama', 'DEM.GEN.PL'],
        ['DEM GEN', 'vayam', 'DEM.GEN'],
        ['DEM PL ACC', 'uyara', 'DEM.ACC.PL'],
        ['DEM ACC', 'vayar', 'DEM.ACC'],
        ['DEM PL DAT', 'uyana', 'DEM.DAT.PL'],
        ['DEM DAT', 'vayan', 'DEM.DAT'],
        ['DEM PL LOC', 'vima', 'DEM.LOC.PL'],
        ['DEM LOC', 'vayim', 'DEM.LOC'],
        ['DEM PL ABL', 'uura', 'DEM.ABL.PL'],
        ['DEM ABL', 'vayur', 'DEM.ABL'],
        ['DEM PL ISTR', 'vira', 'DEM.ISTR.PL'],
        ['DEM ISTR', 'vayir', 'DEM.ISTR'],
        ['DEM PL', 'uyama', 'DEM.PL'],
        ['DEM', 'uyam', 'DEM'],
        ['ITER PL GEN', 'yama', 'Q.GEN.PL'],
        ['ITER GEN', 'yam', 'Q.GEN'],
        ['ITER PL ACC', 'iyara', 'Q.ACC.PL'],
        ['ITER ACC', 'yar', 'Q.ACC'],
        ['ITER PL DAT', 'iyana', 'Q.DAT.PL'],
        ['ITER DAT', 'yan', 'Q.DAT'],
        ['ITER PL LOC', 'yima', 'Q.LOC.PL'],
        ['ITER LOC', 'yim', 'Q.LOC'],
        ['ITER PL ABL', 'iyura', 'Q.ABL.PL'],
        ['ITER ABL', 'yur', 'Q.ABL'],
        ['ITER PL ISTR', 'yira', 'Q.ISTR.PL'],
        ['ITER ISTR', 'yir', 'Q.ISTR'],
        ['ITER PL', 'iyama', 'Q.PL'],
        ['ITER', 'iyam', 'Q'],
        ['ITER PL GEN', 'yama', 'REL.GEN.PL'],
        ['ITER GEN', 'yam', 'REL.GEN'],
        ['ITER PL ACC', 'iyara', 'REL.ACC.PL'],
        ['ITER ACC', 'yar', 'REL.ACC'],
        ['ITER PL DAT', 'iyana', 'REL.DAT.PL'],
        ['ITER DAT', 'yan', 'REL.DAT'],
        ['ITER PL LOC', 'yima', 'REL.LOC.PL'],
        ['ITER LOC', 'yim', 'REL.LOC'],
        ['ITER PL ABL', 'iyura', 'REL.ABL.PL'],
        ['ITER ABL', 'yur', 'REL.ABL'],
        ['ITER PL ISTR', 'yira', 'REL.ISTR.PL'],
        ['ITER ISTR', 'yir', 'REL.ISTR'],
        ['ITER PL', 'iyama', 'REL.PL'],
        ['ITER', 'iyam', 'REL'],
        ['PL VOC', 'a', '-VOC.PL', 'a'],
        ['PL GEN', 'ama', '-GEN.PL', 'ma'],
        ['PL ACC', 'ara', '-ACC.PL', 'ra'],
        ['PL DAT', 'una', '-DAT.PL', 'na'],
        ['PL LOC', 'ima', '-LOC.PL', 'ima'],
        ['PL ABL', 'ura', '-ABL.PL', 'ura'],
        ['PL ISTR', 'ira', '-ISTR.PL', 'ira'],
        ['VOC', 'i', '-VOC', 'i'],
        ['GEN', 'am', '-GEN', 'm'],
        ['ACC', 'ar', '-ACC', 'r'],
        ['DAT', 'un', '-DAT', 'n'],
        ['LOC', 'im', '-LOC', 'im'],
        ['ABL', 'ur', '-ABL', 'ur'],
        ['ISTR', 'ir', '-ISTR', 'ir'],
        ['COMP', 'nu', '-COMP', 'nu'],
        ['SUPL', 'va', '-SUPL', 'va'],
        ['1 SBJV', 'ur', '-SBJV.1SG'],
        ['2 SBJV', 'un', '-SBJV.2SG'],
        ['1 PL SBJV', 'ura', '-SBJV.1PL'],
        ['2 PL SBJV', 'una', '-SBJV.2PL'],
        ['PL PL SBJV', 'uma', '-SBJV.3PL'],
        ['SBJV', 'um', '-SBJV.3SG'],
        ['PL PL', 'ama', '-3PL'],
        ['1 PL', 'ara', '-1PL'],
        ['2 PL', 'ana', '-2PL'],
        ['1', 'ar', '-1SG'],
        ['2', 'an', '-2SG'],
        ['', 'am', '-3SG'],
        ['1 FUT SBJV', 'uri', '-SBJV.FUT.1SG'],
        ['2 FUT SBJV', 'uni', '-SBJV.FUT.2SG'],
        ['1 PL FUT SBJV', 'uray', '-SBJV.FUT.1PL'],
        ['2 PL FUT SBJV', 'unay', '-SBJV.FUT.2PL'],
        ['PL PL FUT SBJV', 'umay', '-SBJV.FUT.3PL'],
        ['FUT SBJV', 'vi', '-SBJV.FUT.3SG'],
        ['PL PL FUT', 'amay', '-FUT.3PL'],
        ['1 PL FUT', 'aray', '-FUT.1PL'],
        ['2 PL FUT', 'anay', '-FUT.2PL'],
        ['1 FUT', 'ari', '-FUT.1SG'],
        ['2 FUT', 'ani', '-FUT.2SG'],
        ['FUT', 'ay', '-FUT.3SG'],
        ['sha', 'sha', '-and', 'sha'],
        ['kuum', 'yul', '-CARD', 'yul'],
        ['PL', 'a', '-PL', 'a'],
        ['yisun', '', 'DTV'],
        ['DEM', 'uyami', 'DET'],
        ['ablikal', 'ablikal', 'BEN'],
        ['um', 'um', 'NEG']]

length = {'aa': 'ā',
          'ii': 'ī',
          'uu': 'ū'}

vowels = ('a', 'e', 'i', 'o', 'u')

v = ['a', 'a', 'a', 'a', 'a', 'a',
     'aa', 'aa', 'aa',
     'i', 'i', 'i', 'i', 'i',
     'ii', 'ii',
     'u', 'u', 'u', 'u',
     'uu']

c = ['p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p',
     'b', 'b', 'b',
     't', 't', 't', 't', 't', 't', 't', 't', 't', 't', 't', 't',
     'd', 'd', 'd', 'd',
     'ch', 'ch', 'ch', 'ch', 'ch', 'ch', 'ch', 'ch', 'ch',
     'j',
     'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k',
     'g', 'g',
     'v', 'v', 'v', 'v', 'v', 'v', 'v',
     's', 's', 's', 's', 's', 's', 's', 's',
     'sh', 'sh', 'sh', 'sh', 'sh',
     'h', 'h', 'h', 'h', 'h', 'h']

r = ['v', 'v',
     'r', 'r', 'r', 'r',
     'l', 'l', 'l',
     'y']

k = ['m', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm',
     'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n',
     'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p',
     'b', 'b', 'b',
     't', 't', 't', 't', 't', 't', 't', 't', 't', 't', 't', 't',
     'd', 'd', 'd', 'd',
     'ch', 'ch', 'ch', 'ch', 'ch', 'ch', 'ch', 'ch', 'ch',
     'j',
     'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k', 'k',
     'g', 'g',
     'v', 'v', 'v', 'v', 'v', 'v', 'v',
     's', 's', 's', 's', 's', 's', 's', 's',
     'sh', 'sh', 'sh', 'sh', 'sh',
     'h', 'h', 'h', 'h', 'h', 'h'
     'v', 'v', 'v', 'v', 'v', 'v', 'v', 'v', 'v', 'v', 'v', 'v', 'v',
     'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r',
     'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l', 'l',
     'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y']

lexicon = open('lexicon.csv')
reader = csv.reader(lexicon)
dictlist = list(reader)
lexicon.close()

select = None

transclst = []

imagename = None

blogTitle = None

print('\nWelcome to Triva: Tools for translating Triva Lapur')

inputFrame = Frame(root)
inputFrame.pack()

sentenceLabel = Label(inputFrame, text = 'Input Sentence:')
sentenceLabel.grid(row = 0, column = 0, sticky = E, pady = 50, padx = 10)

sentence = Entry(inputFrame, width = 140)
sentence.grid(row = 0, column = 1)

glossLabel = Label(inputFrame, text = 'Input Gloss:')
glossLabel.grid(row = 1, column = 0, sticky = E, padx = 10)

gloss = Entry(inputFrame, width = 140)
gloss.grid(row = 1, column = 1)

glossButton = Button(inputFrame, text = 'Update', command = translit)
glossButton.grid(row = 1, column = 2, padx = 5)

translitLabel = Label(inputFrame, text = 'Transliteration:')
translitLabel.grid(row = 2, column = 0, sticky = E, padx = 10)

tl = StringVar()

translitDisplay = Label(inputFrame, textvariable = tl)
translitDisplay.grid(row = 2, column = 1, sticky = W, pady = 50)

transcrButton = Button(inputFrame, text = 'Transcr.', command = transc)
transcrButton.grid(row = 2, column = 2, padx = 5)

transcrLabel = Label(inputFrame, text = 'Transcription:')
transcrLabel.grid(row = 3, column = 0, sticky = E, padx = 10)

transcrDisplay = Label(inputFrame)
transcrDisplay.grid(row = 3, column = 1, sticky = W)

exportButton = Button(inputFrame, text = 'Export', command = blog)
exportButton.grid(row = 3, column = 2, padx = 5)

choice = []

if os.path.isfile("mydata"):
    shelfFile = shelve.open('mydata')
    choice = shelfFile['choice']
    sentence.insert(0, shelfFile['sentence'])
    gloss.insert(0, shelfFile['gloss'])
    shelfFile.close()
else:
    sentence.insert(0, "dummy")
    gloss.insert(0, "dummy")
translit()

editFrame = Frame(root)
editFrame.pack(side = LEFT)

rndButton = Button(editFrame, text = 'Random', command = generate)
rndButton.grid(row = 0, column = 0, padx = 10, pady = 50)

rand = StringVar()

rndDisplay = Label(editFrame, textvariable = rand)
rndDisplay.grid(row = 0, column = 1, sticky = W)

entryLabel = Label(editFrame, text = 'Entry:')
entryLabel.grid(row = 1, column = 0, sticky = E, padx = 10)

dictentry = Entry(editFrame, width = 30)
dictentry.grid(row = 1, column = 1)

deleteButton = Button(editFrame, text = 'Delete', command = delete)
deleteButton.grid(row = 1, column = 2, padx = 5)

grammarLabel = Label(editFrame, text = 'Grammar:')
grammarLabel.grid(row = 2, column = 0, sticky = E, padx = 10, pady = 50)

grammar = Entry(editFrame, width = 30)
grammar.grid(row = 2, column = 1)

charLabel = Label(editFrame, text = 'Character:')
charLabel.grid(row = 3, column = 0, sticky = E, padx = 10)

char = Entry(editFrame, width = 30)
char.grid(row = 3, column = 1)

defLabel = Label(editFrame, text = 'Definition:')
defLabel.grid(row = 4, column = 0, sticky = E, padx = 10, pady = 50)

definition = Entry(editFrame, width = 30)
definition.grid(row = 4, column = 1)

addButton = Button(editFrame, text = 'Add', command = add)
addButton.grid(row = 4, column = 2, padx = 5, pady = 50)

root.mainloop()
